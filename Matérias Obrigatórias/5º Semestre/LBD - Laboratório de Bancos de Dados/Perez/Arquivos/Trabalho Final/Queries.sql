﻿INSERT INTO disponibilidade(data,hora,crm) VALUES ('1/8/2010', 12, 1);
INSERT INTO disponibilidade(data,hora,crm) VALUES ('1/8/2010', 13, 1);
INSERT INTO disponibilidade(data,hora,crm) VALUES ('1/5/2010', 12, 1);
INSERT INTO disponibilidade(data,hora,crm) VALUES ('1/7/2010', 9, 5);


--funcao que apaga as disponibilidades de dias passados
CREATE OR REPLACE FUNCTION apagaDatasAntigas() RETURNS VOID AS $$	
	BEGIN
		DELETE 
		FROM disponibilidade 
		WHERE data < current_date;
	END;
$$ LANGUAGE 'plpgsql';
ALTER FUNCTION apagaDatasAntigas() OWNER TO postgres;
SELECT * FROM apagaDatasAntigas();


--criar atendimentos
CREATE OR REPLACE FUNCTION atendimentos() RETURNS VOID AS $$
	DECLARE
	i integer;
	BEGIN
		FOR i IN 1..5 LOOP
		INSERT INTO atende(m_crm,esp_id) VALUES (i,i+1);
		END LOOP;
	END;
$$ LANGUAGE 'plpgsql';
ALTER FUNCTION atendimentos() OWNER TO postgres;
SELECT * FROM atendimentos();

--criar disponibilidade
CREATE OR REPLACE FUNCTION criar_disponibilidade() RETURNS VOID AS $$	
	DECLARE
		i integer;
		d DATE;
		t integer;
	BEGIN
		FOR i IN 1..1000 LOOP
		d = CURRENT_DATE + I;
		t = date_part('hour', timestamp '2001-02-16 20:38:40'); 
		INSERT INTO disponibilidade(data, hora, crm) VALUES (d,t+1,3);
		END LOOP;
	END;
$$ LANGUAGE 'plpgsql';
ALTER FUNCTION criar_disponibilidade() OWNER TO postgres;
SELECT * FROM criar_disponibilidade();

--obtem as especialidades 
SELECT nome, id
FROM especialidade;

--obter medicos por especialidade
SELECT m.nome
FROM medico AS m, atende AS a
WHERE a.e_id = 5 AND a.m_crm = m.crm

--retorna as disponibilidades do medico
SELECT m.nome, d.data, d.hora
FROM medico as m, disponibilidade as d
WHERE m.crm = d.crm;

--view todos os medicos e suas especialidades
create view medico_especialidade (medicoNome,especialidade)as
select m.nome,e.nome
from medico as m, especialidade as e,atende as a
where e.id = a.e_id and a.m_crm = m.crm

--indice por medico tabela disponibilidade
create index crmDispo on disponibilidade(crm)

--regra do horario
create or replace rule horario_rule as
	on insert to disponibilidade
	where hora > 24 or hora < 0
	do instead nothing;

--triger para apagar disponibilidade na criação da consulta
CREATE OR REPLACE FUNCTION apagaDispo() RETURNS TRIGGER AS $disponibilidade$	
	BEGIN	
		IF (TG_OP = 'INSERT') THEN
		DELETE 
		FROM disponibilidade 
		WHERE crm = NEW.crm and data = NEW.data and hora = NEW.hora;
		RETURN NEW;
		END IF;
		IF (TG_OP = 'DELETE') THEN
		INSERT INTO disponibilidade(data, hora, crm) values(OLD.data, OLD.hora, OLD.crm);
		RETURN OLD;		
		END IF;
	END;
$disponibilidade$ LANGUAGE 'plpgsql';

CREATE TRIGGER organiza_dispo
AFTER insert or delete ON consulta
FOR EACH ROW EXECUTE PROCEDURE apagaDispo();

--view o medico e a especialidade disponiveis no dia
create view medico_dia(medicoNome,especialidade)as
select m.nome,e.nome
from medico as m, especialidade as e,atende as a, disponibilidade as d
where e.id = a.e_id and a.m_crm = m.crm and d.crm = a.m_crm and d.data = current_date

drop view medico_dia
--testes
select * from disponibilidade where crm = 3;

insert into consulta values (current_date, 14, true, 3, 1111, 1)

insert into disponibilidade values (current_date, 14, 4)

delete from consulta where crm = 3;

SELECT * FROM CONSULTA 

select distinct * from medico_dia