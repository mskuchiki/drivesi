﻿
INSERT INTO EMPRESA.EMPREGADO
	VALUES('JOHN','B','SMITH','123456789','1965-10-09','HOUSTON, TX','M',30000,333445555,5),
	      ('FRANKLIN','T','WONG','333445555','1965-12-08','HOUSTON, TX','M',40000,888665555,5),
	      ('ALICIA','J','ZELAYA','999887777','1965-01-19','SPRING, TX','F',25000,987654321,4),
	      ('JENNIFER','S','WALLACE','987654321','1965-06-20','BELLAIRE, TX','F',43000,888665555,4),
	      ('RAMESH','K','NARAYAN','666884444','1965-09-15','HUMBLE, TX','M',38000,333445555,5),
	      ('JOYCE','A','ENGLISH','453453453','1965-07-31','HOUSTON, TX','F',25000,333445555,5),
	      ('AHMAD','V','JABBAR','987987987','1965-03-29','HOUSTON, TX','M',25000,987654321,4),
	      ('JAMES','E','BORG','888665555','1965-11-10','HOUSTON, TX','M',55000,999999999,1);
INSERT INTO EMPRESA.DEPT_LOCALIZACOES
VALUES(1,'HOUSTON'),
      (4,'STAFFORD'),
      (5,'BELLAIRE'),
      (5,'SUGARLAND'),
      (3,'HOUSTON');
INSERT INTO EMPRESA.DEPARTAMENTO
VALUES('PESQUISA',5,'333445555','1988-05-22'),
	('ADMINISTRACAO',4,'987654321','1995-01-01'),
	('SEDE',1,'888665555','1981-06-19');
INSERT INTO EMPRESA.TRABALHA_EM
VALUES ('123456789',1,32.5),
	('123456789',2,7.5),
	('666884444',3,40.0),
	('453453453',1,20.0),
	('453453453',2,20.0),
	('333445555',2,10.0),
	('333445555',3,10.0),
	('333445555',10,10.0),
	('333445555',20,10.0),
	('999887777',30,30.0),
	('999887777',10,10.0),
	('987987987',10,35.0),
	('987987987',30,5.0),
	('987654321',30,20.0),
	('987654321',20,15.0),
	('888665555',20,30);
INSERT INTO EMPRESA.PROJETO
VALUES ('PRODUTOX',1,'BELLAIRE',5),
	('PRODUTOY',2,'SUGARLAND',5),
	('PRODUTOZ',3,'HOUSTON',5),
	('AUTOMATIZACAO',10,'STAFFORD',4),
	('REORGANIZACAO',20,'HOUSTON',1),
	('NOVOS BEN',30,'STAFFORD',4);
INSERT INTO EMPRESA.DEPENDENTE
VALUES ('333445555','ALICE','F','1986-04-05','FILHA'),
	('333445555','THEODORE','M','1983-10-25','FILHO'),
	('333445555','JOY','F','1958-05-03','CONJUGE'),
	('987654321','ABNER','M','1942-02-28','CONJUGE'),
	('123456789','MICHAEL','M','1988-01-04','FILHO'),
	('123456789','ALICE','F','1988-12-30','FILHA'),
	('123456789','ELIZABETH','F','1967-05-05','CONJUGE');